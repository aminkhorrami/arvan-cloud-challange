import Vue from 'vue';

import App from './app/App.vue';
import store from './app/store/app.store';
import router from './app/router/app.router';
import apiConstructor from './app/services/base.api.config';
import { installVendorPlugins } from './app/vendor';

import { CHECK_AUTH } from 'app/store/actions.types';
import vuetify from 'plugins/vuetify.plugin';

import 'styles/app.scss';

//Keep user logged in over the app
//@ts-ignore
router.beforeEach((to, from, next) => Promise.all([store.dispatch(CHECK_AUTH)]).then(next));
export const eventBus = new Vue();
installVendorPlugins();
apiConstructor.init();
const options = {
  el: '#app',
  router,
  store,
  vuetify,
  render: create => create(App)
};

new Vue(options);
