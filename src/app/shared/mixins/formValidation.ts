export default {
  data: () => ({
    rules: {
      required: (value: any) => !!value || 'Required field',
      email: (value: any) => {
        const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(value) || 'Invalid e-mail.';
      },
      min: (value: any) => {
        if (value) {
          return value.length >= 8 || 'At least 8 characters';
        } else {
          return 'Provide a pass';
        }
      }
    }
  })
};
