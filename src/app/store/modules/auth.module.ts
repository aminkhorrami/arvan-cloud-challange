import { LOGIN, LOGOUT, REGISTER, CHECK_AUTH } from 'app/store/actions.types';
import { setUser, logout, setError } from 'app/store/mutations.types';
import ApiService from 'app/services/base.api.config';
import storageService from 'app/config/storage.config';

const state = {
  errors: {},
  user: {},
  isAuthenticated: Boolean(storageService.getToken())
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  }
};

const actions = {
  [LOGIN](context, credentials) {
    return ApiService.post('users/login', { user: credentials })
      .then(({ data }) => {
        context.commit(setUser, data.user);
        return data;
      })
      .catch(({ response }) => {
        context.commit(setError, response.data.errors);
      });
  },
  [LOGOUT](context) {
    context.commit(logout);
  },
  [REGISTER](context, credentials) {
    return ApiService.post('users', { user: credentials })
      .then(({ data }) => {
        context.commit(setUser, data.user);
        return data;
      })
      .catch(({ response }) => {
        context.commit(setError, response.data.errors);
      });
  },
  [CHECK_AUTH](context) {
    if (storageService.getToken()) {
      ApiService.setHeader();
      ApiService.get('user')
        .then(({ data }) => {
          context.commit(setUser, data.user);
        })
        .catch(({ response }) => {
          context.commit(setError, response.data.errors);
        });
    } else {
      context.commit(logout);
    }
  }
};

const mutations = {
  [setError](state, error) {
    state.errors = error;
  },
  [setUser](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
    storageService.saveToken(state.user.token);
  },
  [logout](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    storageService.destroyToken();
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
