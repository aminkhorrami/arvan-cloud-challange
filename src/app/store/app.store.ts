import Vue from 'vue';
import Vuex from 'vuex';
import authentication from 'app/store/modules/auth.module';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    authentication
  }
});
