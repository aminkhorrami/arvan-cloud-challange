// USER AUTH
export const setUser = 'setUser';
export const logout = 'logOut';
// ARTICLE MUTTATIONS
export const setArticles = 'setArticles';
export const setLoading = 'setLoading';
export const setArticle = 'setArticle';
export const setError = 'setError';
export const setTags = 'setTags';
export const addTag = 'addTag';
export const removeTag = 'removeTag';
export const resetModuleState = 'resetModuleState';
export const setUpdateArticles = 'setUpdateArticles';
