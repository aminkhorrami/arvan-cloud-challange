// AUTH ACTION NAMES
export const CHECK_AUTH = 'checkAuth';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const REGISTER = 'register';

// ARTICLE (actions due to the api doc)
export const PUBLISH_ARTICLE = 'publishArticle';
export const DELETE_ARTICLE = 'deleteArticle';
export const EDIT_ARTICLE = 'editArticle';
export const FETCH_ARTICLE = 'fetchArticle';
export const FETCH_ARTTCLES = 'fetchArticles';
export const UPADTE_ARTICLES = 'updateArticles';

// TAGS
export const ADD_TAG_TO_ARTICLE = 'addTagToArticle';
export const REMOVE_TAG_FROM_ARTICLE = 'removeTagFromArticle';
export const FETCH_TAGS = 'fetchTags';
