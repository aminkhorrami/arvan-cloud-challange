const tokenKeyOnLocalStorage: string = 'token';

export const getToken = () => {
  return window.localStorage.getItem(tokenKeyOnLocalStorage);
};

export const saveToken = (token: string): void => {
  window.localStorage.setItem(tokenKeyOnLocalStorage, token);
};

export const destroyToken = (): void => {
  window.localStorage.removeItem(tokenKeyOnLocalStorage);
};

export default { getToken, saveToken, destroyToken };
