//@ts-nocheck
import Vue from 'vue';
import axios, { AxiosStatic, AxiosPromise, AxiosRequestConfig } from 'axios';

import Jwt from 'app/common/storage';
import { API_URL } from 'app/config/base.config';

interface functionResolver<T, U, Z> {
  (resource: T, params: U, slug: Z): AxiosPromise<any>;
}

interface ApiService {
  init: () => void;
  setHeader: () => void;
  query: functionResolver<>;
  get: functionResolver<>;
  post: functionResolver<>;
  update: functionResolver<>;
  put: functionResolver<>;
  delete: functionResolver<>;
}

const ApiService: ApiService = {
  init() {
    Vue.use(axios);
    axios.defaults.baseURL = API_URL;
  },

  setHeader() {
    axios.defaults.headers.common['Authorization'] = `Token ${Jwt.getToken()}`;
  },

  async query(resource, params) {
    return axios.get(resource, params).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  async get(resource, slug = '') {
    return axios.get(`${resource}/${slug}`).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  post(resource, params) {
    return axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return axios.put(`${resource}`, params);
  },

  async delete(resource) {
    return axios.delete(resource).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  }
};
export default ApiService;
