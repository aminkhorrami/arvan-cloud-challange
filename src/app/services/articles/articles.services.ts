import ApiService from 'app/services/base.api.config';

export const ArticlesService = {
  query(params: any) {
    return ApiService.query('articles?offset=0&limit=50', {
      params: params
    });
  },
  get(slug: any) {
    return ApiService.get('articles', slug);
  },
  create(params: any) {
    return ApiService.post('articles', { article: params });
  },
  update(slug: any, params: any) {
    return ApiService.update('articles', slug, { article: params });
  },
  destroy(slug: any) {
    return ApiService.delete(`articles/${slug}`);
  }
};
