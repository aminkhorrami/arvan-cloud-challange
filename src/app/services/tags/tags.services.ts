import ApiService from 'app/services/base.api.config';

export const TagsService = {
  get() {
    return ApiService.get('tags');
  }
};
