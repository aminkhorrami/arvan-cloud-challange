import Vue from 'vue';
import VueRouter from 'vue-router';
import storage from 'app/config/storage.config';
import authRouter from './auth.router';
import NotFound from 'views/NotFound.vue';
Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    redirect: { name: '404' }
  },
  {
    path: '/404',
    name: '404',
    component: NotFound
  },
  ...authRouter()
];

const router = new VueRouter({
  mode: 'history',
  base: '/login',
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let user = storage.getToken();
    if (user) {
      next();
    } else {
      next({ name: 'Login' });
    }
  } else {
    next();
  }
});

export default router;
