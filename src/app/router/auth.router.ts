import { RouteConfig } from 'vue-router';
import Login from '/app/views/Login.vue';
import Register from 'app/views/Register.vue';

export default (): RouteConfig[] => {
  return [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    }
  ];
};
